# daverona/compose/dnsmasq

## Prerequisites

### macOS

```bash
sudo mkdir -p /etc/resolver
echo "nameserver 127.0.0.1" \
  | sudo tee /etc/resolver/example \
  | sudo tee /etc/resolver/invalid \
  | sudo tee /etc/resolver/localhost \
  | sudo tee /etc/resolver/test
```

### Ubuntu

```bash
sudo systemctl disable systemd-resolved
sudo systemctl stop systemd-resolved
sudo rm /etc/resolv.conf
echo "nameserver 127.0.0.1" | sudo tee -a /etc/resolv.conf
echo "nameserver 8.8.8.8" | sudo tee -a /etc/resolv.conf
```

## Quick Start

```bash
cp .env.example .env
# edit .env
docker-compose up --detach
```

Among other things, observe every domain ending with `example`, `invalid`, `localhost`, 
or `test` is mapped to your host (*not* the container):

```bash
ping -c 3 a.new.hope.example
ping -c 3 the.empire.strikes.back.invalid
ping -c 3 return.of.the.jedi.localhost
ping -c 3 now.what.test
```

Why these four? Read [reserved domains](https://en.wikipedia.org/wiki/Top-level_domain#Reserved_domains).
What's wrong with `dev` domain? Ask [Google](https://www.google.com/search?q=who+owns+dev+domain&rlz=1C5CHFA_enKR1090KR1090&oq=who+owns+dev+domain&gs_lcrp=EgZjaHJvbWUyBggAEEUYOTIGCAEQABge0gEINDMwM2owajSoAgCwAgA&sourceid=chrome&ie=UTF-8).

## References

* Dnsmasq: [http://www.thekelleys.org.uk/dnsmasq/doc.html](http://www.thekelleys.org.uk/dnsmasq/doc.html)
* Reserved domains: [https://en.wikipedia.org/wiki/Top-level\_domain#Reserved\_domains](https://en.wikipedia.org/wiki/Top-level_domain#Reserved_domains)
* Local development with virtual hosts and HTTPS: [https://ianduffy.ie/blog/2019/02/22/local-development-with-virtual-hosts-and-https/](https://ianduffy.ie/blog/2019/02/22/local-development-with-virtual-hosts-and-https/)
* Dnsmasq repository: [http://www.thekelleys.org.uk/dnsmasq/](http://www.thekelleys.org.uk/dnsmasq/)
* [https://computingforgeeks.com/install-and-configure-dnsmasq-on-ubuntu-18-04-lts/](https://computingforgeeks.com/install-and-configure-dnsmasq-on-ubuntu-18-04-lts/)
